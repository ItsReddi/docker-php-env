#!/bin/sh

docker_gitlab_login() {
	echo -n $CI_JOB_TOKEN | docker login -u gitlab-ci-token --password-stdin $CI_REGISTRY
}

docker_hub_login() {
	echo -n $DOCKER_HUB_TOKEN | docker login -u $DOCKER_HUB_USER --password-stdin $DOCKER_HUB_REGISTRY
}

docker_build_push() {
	#1 = 7.0/local/fpm-alpine/Dockerfile Dockerfile location
	#2 = 7.0-fpm-alpine-local-stable - Tag
	#3 = ./7.0 - build context
	echo Login... gitlab
	docker_gitlab_login

	echo Get cache image 
	docker pull "$CI_REGISTRY_IMAGE:$2" || true
	
	echo Building docker image...
	docker build --pull --cache-from "$CI_REGISTRY_IMAGE:$2" --file $1 -t "$CI_REGISTRY_IMAGE:$2" $3

	echo Pushing docker image.. to gitlab
	docker push "$CI_REGISTRY_IMAGE:$2"

	echo Login... Docker Hub
	docker_hub_login

	echo Pushing docker image.. to docker hub
	docker tag "$CI_REGISTRY_IMAGE:$2" "$DOCKER_HUB_REGISTRY_IMAGE:$2"
	docker push "$DOCKER_HUB_REGISTRY_IMAGE:$2"
}


